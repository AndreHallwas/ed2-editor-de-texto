
#include "Bibli.h"

char ListaDE_Letras_Vazia(linha *lin){
	return lin->TamLetras == -1;
}

char ListaDE_Letras_Cheia(linha *lin){
	return lin->TamLetras > MAX_CURSOR_Y;
}

letras* ListaDE_Letras_Inicializa(char *Letra){
    letras *l=(letras*)malloc(sizeof(letras));
    l->ant = NULL;
    l->prox = NULL;
    l->elem = Letra;
    return l;
}

linha* ListaDE_Linha_Inicializa(){
    linha *lin = (linha*)malloc(sizeof(linha));
	lin->inicio = lin->fim = ListaDE_Letras_Inicializa(NULL);
	lin->ant = NULL;
	lin->prox = NULL;
	lin->posy = 0;
	lin->TamLetras = -1;
	return lin;
}


linha* ListaDE_Linha_Inicializa_Primeiro(cursor *c){
    linha *lin = (linha*)malloc(sizeof(linha));
    lin->TamLetras = -1;
	lin->inicio = lin->fim = ListaDE_Letras_Inicializa(NULL);
	lin->ant = NULL;
	lin->prox = NULL;
	lin->posy = 5;/////erro;
	c->cursorLin= lin;
	c->cursorLetra= lin->fim;
	return lin;
}

void ListaDE_LeitorOld(linha *l){
    linha *linAux = l;
    letras *letrasAux;
    char letra;
    while(linAux != NULL){
        letrasAux=linAux->inicio;
        while(letrasAux != NULL){///
            printf("%c",letrasAux->elem);
            letrasAux=letrasAux->prox;
        }
        printf("\n");
        linAux=linAux->prox;
    }
}

void ListaDE_LeitorPorTam(linha *l,char posymin,char posymax,char posxin,char posyin){
    linha *linAux = l;
    letras *letrasAux;
    char letra,contador = 0;
    gotoxy(posxin,posyin);
    while(linAux != NULL && contador < posymax+1){
        if(contador >= posymin){
            letrasAux=linAux->inicio;
            while(letrasAux != NULL){///
                printf("%c",letrasAux->elem);
                letrasAux=letrasAux->prox;
            }
            gotoxy(posxin,++posyin);
        }
        linAux=linAux->prox;
        contador ++ ;
    }
}

void ListaDE_Atualiza_FinalLinha(linha *Linha,letras *Letra){
     Linha->fim = Letra;///Atualiza o Final da Linha;
}


void ListaDE_Insere(cursor *c,char *Letra){
    linha *cLin = c->cursorLin;
    linha *novaLinha;
    letras *cLetras = c->cursorLetra;
    letras *novaLetra = ListaDE_Letras_Inicializa(Letra);
    if(cLin->TamLetras < c->MaxCursorX && Letra != '\n'){

        if(cLetras->prox == NULL){ ///N�o tem Proximo;
            novaLetra->prox = NULL;///N�o h� Proximo;
        }else{
            novaLetra->prox = cLetras->prox;///tem proximo;
        }
        novaLetra->ant = cLetras;///define a anterior;
        cLetras->prox = novaLetra;///define a proxima na anterior;

    }else{
        novaLinha = ListaDE_Linha_Inicializa();
        novaLinha->posy = c->Posy + 1;/////Atualiza posi��o da linha;
        novaLinha->ant = cLin;///A Linha anterior � a do Cursor;

        if(cLin->prox == NULL){///nao tem proxima linha;
            novaLinha->prox = NULL;///N�o tem proxima linha;
        }else{
            novaLinha->prox = cLin->prox;///Tem Proximo E recebe O Proximo;
        }
        if(cLetras->prox == NULL){
        	cLin->prox = novaLinha;///A Proxima Linha do cursor � Esta;
	        novaLinha->inicio->prox = novaLetra;///liga o inicio a nova caixa;
	        novaLetra->ant = novaLinha->inicio;///liga a nova caixa ao inicio;
	        novaLetra->prox = NULL;
	        novaLinha->fim = novaLetra;///O Fim da linha recebe a nova letra;
	        cLin = novaLinha;///cursor recebe nova Linha;
		}else{
			novaLetra->ant = cLetras;
			novaLetra->prox = cLetras->prox;
			cLetras->prox->ant = novaLetra;
			cLetras->prox = novaLetra;
			cLetras = cLetras->prox;

			novaLinha->fim = cLin->fim;
			novaLinha->inicio = cLin->fim;
			cLin->fim = cLin->fim->ant;

			novaLinha->fim->ant = NULL;
			novaLinha->fim->prox = NULL;
			novaLinha->TamLetras++;
		}
        if(Letra == '\n' || Letra == '.'){
            novaLetra->elem = ' ';
        }
    }
    if(cLin->TamLetras < c->MaxCursorX)
    	cLin->TamLetras++;///incrementa o tanto de letras na Linha;

    ListaDE_Atualiza_FinalLinha(cLin,novaLetra);
    ListaDE_Atualiza_Cursor(c,cLin,novaLetra);
}

void ListaDE_VerificaLimiteLinha(cursor *c){
	linha *cLin = c->cursorLin;
	while(cLin != NULL){
		while(cLin->TamLetras >= c->MaxCursorX){
			if(cLin->prox->TamLetras != 0){
				cLin->prox->inicio->ant = cLin->fim;
				cLin->fim = cLin->fim->ant;
				cLin->prox->inicio->ant->ant = NULL;
				cLin->prox->inicio->ant->prox = cLin->prox->inicio;
				cLin->prox->inicio = cLin->prox->inicio->ant;
			}
			cLin->TamLetras--;
			cLin->prox->TamLetras++;
		}
		cLin= cLin->prox;
	}
}

void ListaDE_Remove_Elem_Atras(cursor *c){
    linha *cLin = c->cursorLin;
    letras *cLetras = c->cursorLetra;
    letras *auxRemove = cLetras;

    if(cLetras->ant != NULL){
        cLetras->ant->prox = cLetras->prox;///o anterior recebe o proximo elemento;
        //cLetras->prox->ant = cLetras->ant;///liga o elemento anterior ao proximo;
        cLetras = cLetras->ant;///atualiza o ponteiro do cursor
    }else{
        if(cLin->ant != NULL){/////possivel problema ao remover linha;
            cLin = cLin->ant;///Atualiza o cursor para a Linha Anterior;
            cLetras = cLin->fim;///Atualiza o cursor para A Letra Anterior;
        }else{
            cLetras = NULL;///retira o Cursor Da Letra;
        }
    }
    cLin->TamLetras--;///Decrementa o Tanto de Letras;/////Possivel Problema caso ande para a linha anterior ao apagar letra;
    free(auxRemove);///limpa a variavel;
    ListaDE_Atualiza_Cursor(c,cLin,cLetras);
}


void ListaDE_Remove_Elem_Frente(cursor *c){
    linha *cLin = c->cursorLin;
    letras *cLetras = c->cursorLetra;
    letras *auxRemove = cLetras;

    if(cLetras->prox->prox != NULL){
        cLetras->prox = cLetras->prox->prox;///o elemento atual recebe o proximo do proximo
        cLetras->prox->prox->ant = cLetras;///liga o proximo elemento a este;
        cLetras = cLetras->ant;///atualiza o ponteiro do cursor
    }
    free(auxRemove);///limpa a variavel;
    ListaDE_Atualiza_Cursor(c,cLin,cLetras);
}


char ListaDE_Navegacao_Cima(cursor *c){
    if(c->cursorLin->ant != NULL && c->Posy>4){
        c->cursorLin = c->cursorLin->ant;///o cursor sobe uma posi��o
        c->cursorLetra = c->cursorLin->fim;///o cursor da letra vai para o fim da nova linha;
        return 1;
    }else{
        c->cursorLetra = c->cursorLin->fim;///o cursor letra apenas vai para a primeira letra da linha////
    }
    return 0;
}

char ListaDE_Navegacao_Baixo(cursor *c){
    if(c->cursorLin->prox != NULL && c->Posy <18){
        c->cursorLin = c->cursorLin->prox;///o cursor sobe uma posi��o
        c->cursorLetra = c->cursorLin->inicio;///o cursor da letra vai para o inicio da nova linha;
        return 1;
    }else{
        c->cursorLetra = c->cursorLin->inicio;///o cursor letra apenas vai para a primeira letra da linha
    }
    return 0;
}

char ListaDE_Navegacao_Esquerda(cursor *c){
    if(c->cursorLetra->ant != NULL){
        c->cursorLetra = c->cursorLetra->ant;
        return 1;
    }
    return 0;
}

char ListaDE_Navegacao_Direita(cursor *c){
    if(c->cursorLetra->prox != NULL){
        c->cursorLetra = c->cursorLetra->prox;
        return 1;
    }
    return 0;
}

void ListaDE_Atualiza_Cursor(cursor *c,linha *Linha,letras *Letra){
    c->cursorLin = Linha;///atualiza a linha do cursor;
    c->cursorLetra = Letra;///Atualiza a coluna do cursor;
}

void ListaDE_Pula_Linha(cursor *c){
    linha *cLin = c->cursorLin;
    letras *cLetras = c->cursorLetra;
    linha *auxLin = ListaDE_Linha_Inicializa();
    ///OK////
    auxLin->ant = c->cursorLin;
    auxLin->prox = c->cursorLin->prox;/////
    c->cursorLin->prox = auxLin;

    c->cursorLin = c->cursorLin->prox;
    c->cursorLetra = c->cursorLin->inicio;
}

void ListaDE_Insere_Atualiza(cursor *c,char *Letra){
    char *aux = c->cursorLetra->elem ;
    ///c->cursorLetra->elem = NULL;
    c->cursorLetra->elem = Letra;
    //free(aux);
}
