#ifndef GRAFICO_H
#define GRAFICO_H

struct Size{
	int coli;
	int lini;
	int colf;
	int linf;
	int cort;
	int corf;
};
typedef struct Size size;

size grafico_Add_Size(int coli,int lini,int colf,int linf,int cort,int corf);
void grafico_Add_Quadro(size si);
void grafico_Add_Tracos(size si);
void grafico_Limpa_Tela();
void grafico_Limpa_Rastro(unsigned char x,unsigned char y);
void grafico_Limpa_Quadro(unsigned char x,unsigned char y,unsigned char xf,unsigned char yf);

#endif // TELA_H
