#include <stdio.h>
#include "myconio.h"
#include "Bibli.h"
#include "Grafico.h"
#include "Estruturas.h"

void tela_Pequena_Monta(size si){
	int i=0;
	grafico_Add_Quadro(si);
	for(i=si.coli+1;i<si.colf-1;i+=2){

		gotoxy(i,si.lini+2);printf("- ");

	}
	gotoxy(si.coli+2,si.lini+3);printf("Nome do Arquivo:");
	si=grafico_Add_Size(22,12,58,14,11,0);
	grafico_Add_Quadro(si);
}

void tela_Salvar_Monta(size si){
    grafico_Limpa_Quadro(20,8,60,15);
	tela_Pequena_Monta(si);
	gotoxy(si.coli+2,si.lini+1);printf("Salvar arquivo");
}

void tela_Abrir_Monta(size si){
    grafico_Limpa_Quadro(20,8,60,15);
	tela_Pequena_Monta(si);
	gotoxy(si.coli+2,si.lini+1);printf("Abrir arquivo");
}

void tela_Principal_Monta(size si){
    grafico_Limpa_Tela();
	grafico_Add_Quadro(si);
	tela_Opcoes(si);
	grafico_Add_Tracos(grafico_Add_Size(4,3,75,20,11,0));
}

void tela_Opcoes(size si){
	gotoxy(5,2);printf("F2-Abrir");
	gotoxy(25,2);printf("F3-Salvar");
	gotoxy(45,2);printf("F4-Sair");
	gotoxy(65,2);printf("F5-Exibir ");
	gotoxy(5,21);printf("Lin: ");
	gotoxy(15,21);printf("col: ");
	gotoxy(5,22);printf("Ins: ");
	/////gotoxy(15,22);printf("TamLinha: ");
	gotoxy(25,21);printf("Tecla[F7]-Lista:");
	gotoxy(42,21);printf("Sugestao");
}

void tela_Atualiza_Lin_Col(cursor *c){

    gotoxy(10,21);printf("   ");
	gotoxy(20,21);printf("   ");
	gotoxy(10,21);printf("%d",c->Posy);
	gotoxy(20,21);printf("%d",c->Posx);
	gotoxy(10,22);printf("%d",c->insAtiv);/////
	/////gotoxy(26,22);printf("%d",c->cursorLin->TamLetras);

}

void tela_Atualiza_Sugestao(Pilha *Sugestao,cursor *c){
    gotoxy(42,21);printf("                        ");
    gotoxy(42,21);pilha_listarSemRemover(&Sugestao);
    c->Sugestao = pilha_copiarPalavra(&Sugestao);
}
