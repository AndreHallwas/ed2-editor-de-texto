#ifndef BIBLI_H
#define BIBLI_H

#define TECLA_F2 60
#define TECLA_F3 61
#define TECLA_F4 62
#define TECLA_F5 63
#define TECLA_HOME 71
#define TECLA_END 79
#define TECLA_DEL 83
#define TECLA_INSERT 82
#define TECLA_F7 65
#define TECLA_PAGE_UP 73
#define TECLA_PAGE_DOWN 81
#define TECLA_BACK_SPACE 8
#define TECLA_SETA_CIMA 72
#define TECLA_SETA_BAIXO 80
#define TECLA_SETA_ESQUERDA 75
#define TECLA_SETA_DIREITA 77
#define TECLA_ESC 27
#define TECLA_SPACE 32
#define TECLA_ENTER 13

#define MAX_CURSOR_X 75
#define MAX_CURSOR_Y 18
#define MIN_CURSOR_X 6
#define MIN_CURSOR_Y 5
#define START_CURSOR_X 6
#define START_CURSOR_Y 5

#define LISTA_GEN_CAMINHO "ListaGenPalavrasHistoricoGravadas.txt"

#include <stdio.h>
#include "Grafico.h"
#include "Estruturas.h"

///// definido em Tela.h
void tela_Pequena_Monta(size si);
void tela_Salvar_Monta(size si);
void tela_Abrir_Monta(size si);
void tela_Principal_Monta(size si);
void tela_Opcoes(size si);
void tela_Atualiza_Lin_Col(cursor *c);
void tela_Atualiza_Sugestao(Pilha *Sugestao,cursor *c);

/////definido em tecla.h
char* tecla_Captura();
void tecla_F2(cursor *c,linha *Linha);
void tecla_F3(linha *l,cursor *c);
void tecla_F4(char *Tecla);
void tecla_F5(cursor *c, linha *l);
void tecla_F7(cursor *c);
void tecla_Home(cursor *c);
void tecla_Del(cursor *c);
void tecla_Insert(cursor *c);
void tecla_End(cursor *c);
void tecla_Page_Up(cursor *c);
void tecla_Page_Down(cursor *c);
void tecla_Back_Space(cursor *c);
void tecla_Seta_Cima(cursor *c);
void tecla_Seta_Baixo(cursor *c);
void tecla_Seta_Esquerda(cursor *c);
void tecla_Seta_Direita(cursor *c);
void tecla_Texto(char *Letra,cursor *c);
void tecla_Enter(cursor *c);

///// definido em PilhaDinamica.h
void pilha_Init(Pilha **Palavra);
char pilha_isEmpty(Pilha *Palavra);
char pilha_top(Pilha *Palavra);
void pilha_pop(Pilha **Palavra,char *Letra);
void pilha_push(Pilha **Palavra,char Letra);
void pilha_listar(Pilha **Palavra);
void pilha_listarSemRemover(Pilha **Palavra);
void pilha_gravarSemRemover(Pilha **Palavra ,FILE *arq);
Pilha* criaPalavra(char Palavra[]);
Pilha* pilha_invertePalavra(Pilha* Palavra);
Pilha* pilha_copiarPalavra(Pilha **Palavra);

/////definido em myconio.h
void TextColor(int fontcolor,int backgroundcolor);//simula o textcolor+textbackground
void clrscr();// simula o clrscr()
void gotoxy(int xpos, int ypos);// simula o gotoxy

/////definido em ListaGeneralizada.h
char nula(Listagen *l);
char atomo(Listagen *l);
Listagen* lista_Generalizada_criaElemento(Listagen* cabeca ,Listagen* cauda ,char Letra);
Listagen* lista_Generalizada_inserir(Listagen *l,Pilha* Palavra,Listagen *Anterior);
void lista_Generalizada_exibe(Listagen *l);
void lista_Generalizada_exibePalavrasContrario(Listagen *l ,Pilha *Palavra);
void lista_Generalizada_exibePalavras(Listagen *l ,Pilha *Palavra);
void lista_Generalizada_gravaPalavras(Listagen *l ,Pilha *Palavra ,FILE *arq);
void lista_Generalizada_cosultaPalavra(Listagen *l ,Pilha *Palavra ,Pilha *Sugestao);
void kill(Listagen **l);

/////definido em ListaEncadeada.h
char ListaDE_Letras_Vazia(linha *lin);
char ListaDE_Letras_Cheia(linha *lin);
letras* ListaDE_Letras_Inicializa(char *Letra);
linha* ListaDE_Linha_Inicializa();
linha* ListaDE_Linha_Inicializa_Primeiro(cursor *c);
void ListaDE_LeitorOld(linha *l);
void ListaDE_LeitorPorTam(linha *l,char posymin,char posymax,char posxin,char posyin);
void ListaDE_Atualiza_FinalLinha(linha *Linha,letras *Letra);
void ListaDE_Insere(cursor *c,char *Letra);
void ListaDE_VerificaLimiteLinha(cursor *c);
void ListaDE_Remove_Elem_Atras(cursor *c);
void ListaDE_Remove_Elem_Frente(cursor *c);
char ListaDE_Navegacao_Cima(cursor *c);
char ListaDE_Navegacao_Baixo(cursor *c);
char ListaDE_Navegacao_Esquerda(cursor *c);
char ListaDE_Navegacao_Direita(cursor *c);
void ListaDE_Atualiza_Cursor(cursor *c,linha *Linha,letras *Letra);
void ListaDE_Pula_Linha(cursor *c);
void ListaDE_Insere_Atualiza(cursor *c,char *Letra);

/////definido em Letra.h
void letra_Mostra(char Letra,cursor *c);
void letra_Apaga_Atraz(cursor *c);
void letra_Apaga_Frente(cursor *c);
void letra_Acao(char Letra,cursor *c);

/////definido em Cursor.h
char cursor_Limite_Max_x(cursor *c);
char cursor_Limite_Max_y(cursor *c);
char cursor_Limite_Min_x(cursor *c);
char cursor_Limite_Min_y(cursor *c);
void cursor_Frente(cursor *c);
void cursor_Atras(cursor *c);
void cursor_Baixo(cursor *c);
void cursor_Cima(cursor *c);
cursor* cursor_Inicializa(char MaxCursorX, char MaxCursorY, char MinCursorX, char MinCursorY);
void cursor_SetPosicao(char Posx, char Posy, cursor *c);
void cursor_AtualizaValor(cursor *c);
void cursor_AtualizaPos(cursor *c);
char cursor_Pula_Linha(cursor*c);

/////definido em Arquivo.h
char* arquivo_Pega_Caminho();
FILE* Arquivo_Abrir(char *Caminho);
FILE* arquivo_Abrir();
FILE* arquivo_AbrirTeste();
void arquivo_Ler(cursor *c);
void carrega_ListaPalavras(cursor *c);
void arquivo_MontaLista(char *Letra ,cursor *c ,FILE *arq, char flag ,Pilha *Palavra);
void arquivo_Salvar(linha *l,cursor *c);

#endif
