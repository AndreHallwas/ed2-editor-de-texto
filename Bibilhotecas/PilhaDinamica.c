#include <stdio.h>
#include "Bibli.h"

void pilha_Init(Pilha **Palavra){///Inicializa a pilha dinamica na posi��o nula;
	*Palavra = NULL;
}

char pilha_isEmpty(Pilha *Palavra){////Verifica se a pilha est� vazia;
	return Palavra == NULL;
}

char pilha_top(Pilha *Palavra){//////////Acessa o elemento no topo da pilha;
	if(!pilha_isEmpty(Palavra)){
		return Palavra->info;
	}
	return -1;
}

void pilha_pop(Pilha **Palavra,char *Letra){/////Remove um elemento do topo da pilha;
	Pilha *aux;
	if(!pilha_isEmpty(*Palavra)){
		aux = *Palavra;
		*Letra = (*Palavra)->info;
		*Palavra = aux->prox;
		free(aux);
	}else{
		*Letra = -1;
	}
	return Letra;
}

void pilha_push(Pilha **Palavra,char Letra){//////Insere um elemento no topo da pilha;
	Pilha *aux = (Pilha*)malloc(sizeof(Pilha));
	aux->info = Letra;
	aux->prox = *Palavra;
	*Palavra = aux;
}

void pilha_listar(Pilha **Palavra){////////////Lista todos os elementos da pilha;
	char *Letra = (char*)malloc(1);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra),&Letra);
		printf("%c",Letra);
	}
}

void pilha_listarSemRemover(Pilha **Palavra){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha;
	pilha_Init(&auxPilha);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra) ,&Letra);
		pilha_push(&auxPilha ,Letra);
		printf("%c",Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha ,&Letra);
		pilha_push(&(*Palavra) ,Letra);
	}
}

void pilha_gravarSemRemover(Pilha **Palavra ,FILE *arq){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha;
	pilha_Init(&auxPilha);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra) ,&Letra);
		pilha_push(&auxPilha ,Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha ,&Letra);
		pilha_push(&(*Palavra) ,Letra);
		putc(Letra,arq);
	}
}

Pilha* criaPalavra(char Palavra[]){
    Pilha *NovaPilha ;
    int i = 0;
    strcat(Palavra,"\n");
    pilha_Init(&NovaPilha);
    do{
        i++;
    }while(Palavra[i] != '\n');
    i--;
    while(i>=0){
        pilha_push(&NovaPilha,Palavra[i--]);
    }

    return NovaPilha;
}

Pilha* pilha_invertePalavra(Pilha* Palavra){
    Pilha *NovaPilha;
    char *Letra = (char*)malloc(1);
    pilha_Init(&NovaPilha);
    while(!pilha_isEmpty(Palavra)){
        pilha_pop(&Palavra,&Letra);
		pilha_push(&NovaPilha,Letra);
	}
    return NovaPilha;
}

Pilha* pilha_copiarPalavra(Pilha **Palavra){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha ,*copia;
	pilha_Init(&auxPilha);
	pilha_Init(&copia);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra),&Letra);
		pilha_push(&auxPilha ,Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha,&Letra);
		pilha_push(&(*Palavra) ,Letra);
		pilha_push(&copia ,Letra);
	}
	return copia;
}
