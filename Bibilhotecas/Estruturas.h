#ifndef ESTRUTURAS_H
#define ESTRUTURAS_H

typedef struct pilha Pilha;
typedef struct reg_lista reg_lista;
typedef struct info_lista info_lista;
typedef struct listagen Listagen;
typedef struct Letras letras;
typedef struct Linha linha;
typedef struct Cursor cursor;

//////////////////////////Lista Encadeada/////////////////////////
typedef struct Letras{
    /*
    *Lista Encadeada de Letras;
    */
	char *elem;
	struct Letras *ant;
	struct Letras *prox;
}letras;


typedef struct Linha{
    /*
    *Lista Encadeada de Linhas
    */
	int TamLetras;//Quantidade de Letras na Linha
	letras *inicio;//Inicio da Lista Encadeada de Letras
	letras *fim;//Fim da Lista Encadeada de Letras
	char posy;//Posi��o Vertical Em Que Se Encontra Na Lista;
	struct Linha *ant;
	struct Linha *prox;
}linha;
///////////////////////////////////////////////////////////////////

/////////////////////cursor//////////////////
typedef struct Cursor {
    /*
    *Estrutura Respons�vel por toda a movimenta��o de dados no Programa;
    */
    char Posx;//Posi��o Horizontal do cursor;
    char Posy;//Posi��o Vertical do cursor;
    char PoslI;//Controla a posi��o da primeira linha que ser� exibida na tela ,no caso de h�ver mais do que a quantidade maxima;
    char PoslF;//Controla a posi��o da ultima linha que ser� exibida na tela ,no caso de h�ver mais do que a quantidade maxima;
    char MaxCursorX;//M�xima Posi��o Horizontal do Cursor;
    char MaxCursorY;//M�xima Posi��o Vertical do Cursor;
    char MinCursorX;//Minima Posi��o Horizontal do Cursor;
    char MinCursorY;//Minima Posi��o Vertical do Cursor;
    char insAtiv;//Inser��o de dados Ativa ou N�o;
    char palavraAtiv;//A Palavra est� Sendo Montada ou N�o;
    linha *cursorLin;//Linha da Lista Encadeada em que o cursor se Encontra;
    letras *cursorLetra;//Letra da Lista Encadeada em que o cursor se Encontra;
    Listagen *ListaPalavras;//Ponteiro para Lista Generalizada de Palavras;
    Pilha *Palavra;//Ponteiro para a Palavra que Est� Sendo Digitada;
    Pilha *Sugestao;//Sugest�o da lista Generalizada;
}cursor;
///////////////////////////////////////////////
////////Pilha////////////
typedef struct pilha{
	char info;
	struct pilha *prox;
}Pilha;
/////////////////////////

////////Lista Generalizada////////
typedef struct reg_lista{
	Listagen *cabeca;
	Listagen *cauda;
}reg_lista;

typedef struct info_lista{
	char info;
	reg_lista lista;
}info_lista;

typedef struct listagen{
	char terminal;
	char fim;
	info_lista no;
}Listagen;
/////////////////////////

#endif // ESTRUTURAS_H
