/////#include "myconio.h"
#include "Grafico.h"

size grafico_Add_Size(int coli,int lini,int colf,int linf,int cort,int corf){
	size si;
	si.colf=colf;
	si.coli=coli;
	si.corf=corf;
	si.cort=cort;
	si.linf=linf;
	si.lini=lini;
	return si;
}

void grafico_Add_Quadro(size si){
	int i=0;
	TextColor(si.cort,si.corf);

	gotoxy(si.coli,si.linf);printf("%c",192);
	gotoxy(si.colf,si.lini);printf("%c",191);
	gotoxy(si.coli,si.lini);printf("%c",218);
	gotoxy(si.colf,si.linf);printf("%c",217);

	///Gera Coluna

	for(i=si.coli+1;i<si.colf;i++){

		gotoxy(i,si.lini);printf("%c",196);
		gotoxy(i,si.linf);printf("%c",196);

	}

	///Gera Linha
	for(i=si.lini+1;i<si.linf;i++){

		gotoxy(si.coli,i);printf("%c",179);
		gotoxy(si.colf,i);printf("%c",179);

	}
	gotoxy(1,25);

}

void grafico_Add_Tracos(size si){
	int i=0;

	TextColor(si.cort,si.corf);

	for(i=si.coli+1;i<si.colf;i+=2){

		gotoxy(i,si.lini);printf("- ");
		gotoxy(i,si.linf);printf("- ");

	}

}

void grafico_Limpa_Tela(){
    system("cls");
    gotoxy(1,25);
}

void grafico_Limpa_Rastro(unsigned char x,unsigned char y){
    gotoxy(x,y);
    printf(" ");
    gotoxy(1,25);
}

void grafico_Limpa_Quadro(unsigned char x,unsigned char y,unsigned char xf,unsigned char yf){
    int x1;
    while(y < yf+1)
    {   x1=x;
        while(x1 < xf+1)
        {
            gotoxy(x1,y);
            printf(" ");
            x1++;
        }
        y++;
    }
    gotoxy(1,25);
}
