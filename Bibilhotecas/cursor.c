#ifndef CURSOR_H
#define CURSOR_H

#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <windows.h>
#include "Bibli.h"
#include "Estruturas.h"


char cursor_Limite_Max_x(cursor *c) {
    return c->Posx > c->MaxCursorX-1;
}

char cursor_Limite_Max_y(cursor *c) {
    return c->Posy > c->MaxCursorY-1;
}

char cursor_Limite_Min_x(cursor *c) {
    return c->Posx < c->MinCursorX+1;
}

char cursor_Limite_Min_y(cursor *c) {
    return c->Posy < c->MinCursorY+1;
}

void cursor_Frente(cursor *c) {
    if (!cursor_Limite_Max_x(c)) {
        c->Posx++;
    } else
    if (!cursor_Limite_Max_y(c)) {
        c->Posx = c->MinCursorX;
        c->Posy++;
    }
}

void cursor_Atras(cursor *c) {
    if (!cursor_Limite_Min_x(c)) {
        c->Posx--;
    } else
    if (!cursor_Limite_Min_y(c)) {
        c->Posx = c->cursorLin->TamLetras + c->MinCursorX;
        c->Posy--;
    }
}

void cursor_Baixo(cursor *c) {
    if (!cursor_Limite_Max_y(c)) {
        c->Posy++;
        c->Posx = c->MinCursorX;
    }
}

void cursor_Cima(cursor *c) {
    if (!cursor_Limite_Min_y(c)) {
        c->Posy--;
        c->Posx = c->cursorLin->TamLetras + c->MinCursorX + 1;
    }
}

cursor* cursor_Inicializa(char MaxCursorX, char MaxCursorY, char MinCursorX, char MinCursorY) {
    cursor *c = (cursor*) malloc(sizeof (cursor));
    c->Posx = MinCursorX;
    c->Posy = MinCursorY;
    c->MaxCursorX = MaxCursorX;
    c->MaxCursorY = MaxCursorY;
    c->MinCursorX = MinCursorX;
    c->MinCursorY = MinCursorY;
    c->PoslI = 0;
    c->PoslF = MaxCursorY - MinCursorY;
    c->insAtiv = 0;
    c->ListaPalavras = NULL;
    c->Palavra = NULL;
    c->palavraAtiv = 0;
    c->Sugestao = NULL;
    return c;
}

void cursor_SetPosicao(char Posx, char Posy, cursor *c) {
    c->Posx = Posx;
    c->Posy = Posy;
}

void cursor_AtualizaValor(cursor *c){
    gotoxy(c->Posx,c->Posy);
}

void cursor_AtualizaPos(cursor *c){
    COORD atualizaCursor = {c->Posx , c->Posy};
    SetConsoleCursorPosition( GetStdHandle(STD_OUTPUT_HANDLE), atualizaCursor);
}

char cursor_Pula_Linha(cursor*c){
    if(!cursor_Limite_Max_y(c)){
        c->Posy++;
        c->Posx=c->MinCursorX;
        return 1;
    }
    return 0;
}

#endif // TELA_H


