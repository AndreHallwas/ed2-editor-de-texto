
#include <stdio.h>
#include "Bibli.h"

char nula(Listagen *l){
	return l==NULL;
}

char atomo(Listagen *l){
	return (!nula(l)&&l->terminal);
}

Listagen* lista_Generalizada_criaElemento(Listagen* cabeca ,Listagen* cauda ,char Letra){
    Listagen *Nova = (Listagen*)malloc(sizeof(Listagen));
    Nova->no.info = Letra;
    Nova->no.lista.cabeca = cabeca;
    Nova->no.lista.cauda = cauda;
    return Nova;
}

Listagen* lista_Generalizada_inserir(Listagen *l,Pilha* Palavra,Listagen *Anterior){
    Listagen *auxLista = l;////Declarada auxiliar;
    char *x;
    if(pilha_top(Palavra) != -1){
            if(auxLista!=NULL && auxLista->no.lista.cauda != NULL){
                if(auxLista->no.info < pilha_top(Palavra)){
                    /////Basicamente, se o proximo elemento n�o for nulo, e o anterior tamb�m n�o, e tamb�m n�o houver achado a 					//////letra,ent�o ser� procurada denovo at� achar;
                    auxLista->no.lista.cauda = lista_Generalizada_inserir(l->no.lista.cauda,Palavra,l);/////envio a cauda, a palavra, e a lista atual;
                }else
                if(auxLista->no.info > pilha_top(Palavra)){
                    /////Basicamente se a letra j� for maior que a primeira da lista; Declara uma nova lista insere a letra, a cabe�a 			////////� nula pois ainda n�o existe outro nivel,a cauda ser� a lista atual e atualiza o ponteiro da lista;
                    Listagen *Nova = lista_Generalizada_criaElemento(NULL ,auxLista ,pilha_top(Palavra));/////incrementa;
                    l = Nova;
                    pilha_pop(&Palavra,&x);
                    l->no.lista.cabeca = lista_Generalizada_inserir(l->no.lista.cabeca,Palavra,l);/////envio a cabeca, a palavra, e a lista atual;
                }else{
                    /////Basicamente quando ja for a posi��o certa;
                    pilha_pop(&Palavra,&x);/////Apenas desce ao proximo nivel pois a letra ja existe;
                    auxLista->no.lista.cabeca = lista_Generalizada_inserir(auxLista->no.lista.cabeca,Palavra,auxLista);/////envio a cabeca, a palavra, e a lista atual;
                }
            }else{
                if(auxLista == NULL){
                    /////Basicamente neste nivel da lista n�o h� mais elementos;
                    Listagen *Nova = lista_Generalizada_criaElemento(NULL ,NULL ,pilha_top(Palavra));/////incrementa;
                    l = Nova;///// A Propia Lista Recebe A nova lista deste nivel, pois s� h� ela;
                    pilha_pop(&Palavra,&x);
                    l->no.lista.cabeca = lista_Generalizada_inserir(l->no.lista.cabeca,Palavra,l);/////envio a cauda, a palavra, e a lista atual;
                }else
                if(auxLista->no.info < pilha_top(Palavra)){
                    /////Basicamente, se o proximo elemento n�o for nulo, e o anterior tamb�m n�o, e tamb�m n�o houver achado a 					//////letra,ent�o ser� procurada denovo at� achar;
                    Listagen *Nova = lista_Generalizada_criaElemento(NULL ,NULL ,pilha_top(Palavra));/////incrementa;
                    l->no.lista.cauda = Nova;///// A CauDa Recebe A nova lista
                    pilha_pop(&Palavra,&x);
                    Nova->no.lista.cabeca = lista_Generalizada_inserir(Nova->no.lista.cabeca,Palavra,l);/////envio a cauda, a palavra, e a lista atual;
                }else
                if(auxLista->no.info > pilha_top(Palavra)){
                    /////Basicamente se a letra j� for maior que a primeira da lista; Declara uma nova lista insere a letra, a cabe�a 			////////� nula pois ainda n�o existe outro nivel,a cauda ser� a lista atual e atualiza o ponteiro da lista;
                    l = lista_Generalizada_criaElemento(NULL ,auxLista ,pilha_top(Palavra));/////incrementa;
                    pilha_pop(&Palavra,&x);
                    l->no.lista.cabeca = lista_Generalizada_inserir(l->no.lista.cabeca,Palavra,l);/////envio a cabeca, a palavra, e a lista atual;
                }else{
                    /////Basicamente quando ja for a posi��o certa;
                    pilha_pop(&Palavra,&x);/////Apenas desce ao proximo nivel pois a letra ja existe;
                    auxLista->no.lista.cabeca = lista_Generalizada_inserir(auxLista->no.lista.cabeca,Palavra,l);/////envio a cabeca, a palavra, e a lista atual;
                }
            }
    }else{/////criar caixas sempre com head e tail mesmo vazias, ou colocar retorno;;;;;
        Anterior->fim = 1;/////� O FIM DA PALAVRA
    }
    return l;
}

void lista_Generalizada_exibe(Listagen *l){
    Listagen *auxLista = l;
    if(auxLista!=NULL && auxLista->no.lista.cauda != NULL){
            lista_Generalizada_exibe(auxLista->no.lista.cauda);
    }
     if(auxLista->no.lista.cabeca != NULL){
                lista_Generalizada_exibe(auxLista->no.lista.cabeca);
    }
    printf("%c",auxLista->no.info);
}

void lista_Generalizada_exibePalavrasContrario(Listagen *l ,Pilha *Palavra){
    Listagen *auxLista = l;
    char *x;
    if(auxLista!=NULL && auxLista->no.lista.cauda != NULL){
            lista_Generalizada_exibePalavras(auxLista->no.lista.cauda ,Palavra);
    }
     if(auxLista->no.lista.cabeca != NULL){
                pilha_push(&Palavra,auxLista->no.info);
                lista_Generalizada_exibePalavras(auxLista->no.lista.cabeca ,Palavra);
                pilha_pop(&Palavra,&x);
    }else{
        pilha_push(&Palavra,auxLista->no.info);
        pilha_listarSemRemover(Palavra);
        pilha_pop(&Palavra,&x);
    }
    /////printf("%c",auxLista->no.info);
}

void lista_Generalizada_exibePalavras(Listagen *l ,Pilha *Palavra){
    Listagen *auxLista = l;
    char *x;
    if(auxLista!=NULL){
            if(auxLista->no.lista.cabeca != NULL){
                pilha_push(&Palavra,auxLista->no.info);
                if(auxLista->fim == 1){
                    pilha_listarSemRemover(&Palavra);
                    printf(",");
                }
                lista_Generalizada_exibePalavras(auxLista->no.lista.cabeca ,Palavra);
                pilha_pop(&Palavra,&x);
            }else{
                pilha_push(&Palavra,auxLista->no.info);
                pilha_listarSemRemover(&Palavra);
                printf(",");
                pilha_pop(&Palavra,&x);
            }
            if(auxLista->no.lista.cauda != NULL)
                lista_Generalizada_exibePalavras(auxLista->no.lista.cauda ,Palavra);
    }

    /////printf("%c",auxLista->no.info);
}


void lista_Generalizada_gravaPalavras(Listagen *l ,Pilha *Palavra ,FILE *arq){
    Listagen *auxLista = l;
    char *x;
    if(auxLista!=NULL){
            if(auxLista->no.lista.cabeca != NULL){
                pilha_push(&Palavra,auxLista->no.info);
                if(auxLista->fim == 1){
                    pilha_gravarSemRemover(&Palavra,arq);///////Foi Alterado
                    putc(',',arq);
                }
                lista_Generalizada_gravaPalavras(auxLista->no.lista.cabeca ,Palavra ,arq);
                pilha_pop(&Palavra,&x);
            }else{
                pilha_push(&Palavra,auxLista->no.info);
                pilha_gravarSemRemover(&Palavra,arq);
                putc(',',arq);
                pilha_pop(&Palavra,&x);
            }
            if(auxLista->no.lista.cauda != NULL)
                lista_Generalizada_gravaPalavras(auxLista->no.lista.cauda ,Palavra ,arq);
    }

    /////printf("%c",auxLista->no.info);
}

void lista_Generalizada_cosultaPalavra(Listagen *l ,Pilha *Palavra ,Pilha *Sugestao){
    Listagen *auxLista = l;
    char *x;
    if(auxLista!=NULL){
            if(auxLista->no.info == pilha_top(Palavra)){
                /////Descer / Gravar
                pilha_push(Sugestao,auxLista->no.info);
                pilha_pop(&Palavra,&x);
                lista_Generalizada_cosultaPalavra(auxLista->no.lista.cabeca ,Palavra ,Sugestao);
            }else{
                /////Vai Para o Lado
                if(auxLista->no.lista.cauda != NULL){
                    lista_Generalizada_cosultaPalavra(auxLista->no.lista.cauda ,Palavra ,Sugestao);
                }else{
                    /////Descer / Gravar
                    pilha_pop(&Palavra,&x);
                    pilha_push(Sugestao,auxLista->no.info);
                    lista_Generalizada_cosultaPalavra(auxLista->no.lista.cabeca ,Palavra ,Sugestao);
                }
            }
    }else{
        pilha_Init(&Sugestao);
    }


}

void kill(Listagen **l){
	if(!nula(*l)){
		if(atomo(*l)){
			free(*l);
		}else{
			kill(&(*l)->no.lista.cabeca);
			kill(&(*l)->no.lista.cauda);
			free(*l);
			*l=NULL;
		}
	}
}
