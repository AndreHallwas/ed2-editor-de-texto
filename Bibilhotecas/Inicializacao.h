#include "Bibli.h"
#include "Grafico.h"
#include <locale.h>

void opcoes(cursor *c,char *Tecla,linha *Linha){
    switch((int)*Tecla){
        case TECLA_F2:
            tecla_F2(c,Linha);
        break;

        case TECLA_F3:
            tecla_F3(Linha,c);
        break;

        case TECLA_F4:
            tecla_F4(Tecla);
        break;

        case TECLA_F5:
            tecla_F5(c,Linha);
        break;

        case TECLA_F7:
            tecla_F7(c);
        break;

        case TECLA_HOME:
            tecla_Home(c);
        break;

        case TECLA_DEL:
            tecla_Del(c);
        break;

        case TECLA_INSERT:
            tecla_Insert(c);
        break;

        case TECLA_END:
            tecla_End(c);
        break;

        case TECLA_PAGE_UP:
            tecla_Page_Up(c);
        break;

        case TECLA_PAGE_DOWN://///72
            tecla_Page_Down(c);
        break;

        case TECLA_BACK_SPACE:
            tecla_Back_Space(c);
        break;

        case TECLA_SETA_CIMA:
            tecla_Seta_Cima(c);
        break;

        case TECLA_SETA_BAIXO:
            tecla_Seta_Baixo(c);
        break;

        case TECLA_SETA_ESQUERDA:
            tecla_Seta_Esquerda(c);
        break;

        case TECLA_SETA_DIREITA:
            tecla_Seta_Direita(c);
        break;

        case TECLA_ENTER:
            tecla_Enter(c);
        break;

        default:
            tecla_Texto(Tecla,c);
        break;
    }
    tela_Atualiza_Lin_Col(c);
}

void execucao(cursor *c,linha *l){
    char *Tecla;

    do{
            cursor_AtualizaPos(c);
            Tecla = tecla_Captura();
            opcoes(c,*(&Tecla),l);
            grafico_Limpa_Quadro(2,4,78,18);
            ListaDE_LeitorPorTam(l,c->PoslI,c->PoslF,c->MinCursorX,c->MinCursorY);
            cursor_AtualizaPos(c);
    }while((int)*Tecla != TECLA_ESC);
    gotoxy(1,24);
}

void inicializar(){
    cursor *c = cursor_Inicializa(65,18,5,4);
    linha *l = ListaDE_Linha_Inicializa_Primeiro(c);
    system("mode con:cols=82 lines=26");
    tela_Principal_Monta(grafico_Add_Size(1,1,79,24,13,0));
    execucao(c,l);
    free(c);
    free(l);
}

