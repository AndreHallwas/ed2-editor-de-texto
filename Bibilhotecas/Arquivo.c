
#include <stdio.h>
#include <stdlib.h>
#include "Bibli.h"
#include "Estruturas.h"

char* arquivo_Pega_Caminho(){
    char *Caminho;
    Caminho = (char*)malloc(100);
    gotoxy(24,13);
    gets(Caminho);
    return Caminho;
}

FILE* Arquivo_Abrir(char *Caminho){
	return fopen(Caminho,"a+");
}

FILE* arquivo_Abrir(){
    tela_Abrir_Monta(grafico_Add_Size(20,8,60,15,11,0));
    return Arquivo_Abrir(arquivo_Pega_Caminho());
}

FILE* arquivo_AbrirTeste(){
    /////tela_Abrir_Monta(grafico_Add_Size(20,8,60,15,11,0));
    return Arquivo_Abrir("Texto.txt");
}

void arquivo_Ler(cursor *c){
    FILE*arq = arquivo_Abrir();
    char *Letra = (char*)malloc(1);
    Pilha *Palavra = NULL;
    fseek(arq,0,0);
    /////carrega_ListaPalavras(c);
    arquivo_MontaLista(Letra,c,arq,0,Palavra);
    Palavra = NULL;
    //lista_Generalizada_exibePalavras(c->ListaPalavras,Palavra);
    ///lista_Generalizada_exibePalavras(c->ListaPalavras,Palavra);
    lista_Generalizada_gravaPalavras(c->ListaPalavras,Palavra,fopen("ListaGenPalavrasHistoricoGravadas.txt","w+"));
    lista_Generalizada_gravaPalavras(c->ListaPalavras,Palavra,fopen("ListaGenPalavrasHistoricoGravadas.txt","w+"));
    lista_Generalizada_gravaPalavras(c->ListaPalavras,Palavra,fopen("ListaGenPalavrasHistoricoGravadas.txt","w+"));
    fseek(arq,0,0);
    while(!feof(arq)){
        Letra = getc(arq);
        ListaDE_Insere(c , Letra);
        cursor_Frente(c);
    }
    fclose(arq);
}


void carrega_ListaPalavras(cursor *c){
    FILE *arq = fopen(LISTA_GEN_CAMINHO,"a+");
    char *Letra = (char*)malloc(1);
    Pilha *Palavra = NULL;
    fseek(arq,0,0);
    arquivo_MontaLista(Letra,c,arq,0,Palavra);
    fclose(arq);
}

void arquivo_MontaLista(char *Letra ,cursor *c ,FILE *arq, char flag ,Pilha *Palavra){
        if(!feof(arq)){
            Letra = getc(arq);
            /////printf("%c",Letra);
            if(flag == 1){
                if(Letra>62){/////Letra>96 && Letra<123 || Letra>64 && Letra<91 || Letra>127 && Letra<170);
                    /////adiciona
                    flag = 1;
                    pilha_push(&Palavra,Letra);
                    arquivo_MontaLista(Letra,c,arq,flag,Palavra);
                }else{

                    /////finaliza string e grava na lista de palavras

                    c->ListaPalavras = lista_Generalizada_inserir(c->ListaPalavras ,pilha_invertePalavra(Palavra) ,c->ListaPalavras);
                    /////free(Palavra);
                    Palavra = NULL;
                    flag = 0;
                    arquivo_MontaLista(Letra,c,arq,flag,Palavra);
                }
            }else{
                if(Letra>62){
                    /////cria string
                    pilha_push(&Palavra,Letra);
                    flag = 1;
                    arquivo_MontaLista(Letra,c,arq,flag,Palavra);
                }else{
                    arquivo_MontaLista(Letra,c,arq,flag,Palavra);
                }
            }

        }
}

void arquivo_Salvar(linha *l,cursor *c){
    tela_Salvar_Monta(grafico_Add_Size(20,8,60,15,11,0));
    FILE*arq = fopen(arquivo_Pega_Caminho(),"w");
    char *Letra = (char*)malloc(1);
    linha *auxLinha = l;
    letras *auxLetras;
    fseek(arq,0,0);
    while(auxLinha != NULL){
        auxLetras = auxLinha->inicio;
        while(auxLetras != NULL){
            putc(auxLetras->elem,arq);
            auxLetras = auxLetras->prox;
        }
        putc('\n',arq);
        auxLinha = auxLinha->prox;
    }
    fclose(arq);

}
