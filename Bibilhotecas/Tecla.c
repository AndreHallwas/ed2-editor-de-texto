
#include "Bibli.h"
#include "Grafico.h"
#include "Estruturas.h"


char* tecla_Captura(){
	char *aux = (char*)malloc(1);
	char tecla;
	while(!kbhit()){
	}
	tecla=getch();
	if(tecla==-32||tecla==0){
		tecla=getch();
	}
	*aux=tecla;
	return aux;
}

void tecla_F2(cursor *c,linha *Linha){
    //tela_Abrir_Monta(grafico_Add_Size(20,8,60,15,11,0));
    arquivo_Ler(c);
    c->cursorLin = Linha;
    c->cursorLetra = Linha->inicio;
    c->Posx = c->MinCursorX;
    c->Posy = c->MinCursorY;
    cursor_AtualizaPos(c);
}

void tecla_F3(linha *l,cursor *c){
    arquivo_Salvar(l,c);
}

void tecla_F4(char *Tecla){
    *Tecla = 27;
}

void tecla_F5(cursor *c, linha *l){
    ListaDE_LeitorPorTam(l,c->PoslI,c->PoslF,c->MinCursorX,c->MinCursorY);
}

void tecla_F7(cursor *c){
    Pilha *Sugestao = c->Sugestao;
    char *Letra  = (char*)malloc(1);
    while(c->cursorLetra->elem > 63 && c->cursorLetra->ant != NULL){
        c->cursorLetra->ant->prox = c->cursorLetra->prox;
        if(c->cursorLetra->prox != NULL)
            c->cursorLetra->prox->ant = c->cursorLetra->ant;
        cursor_Atras(c);
        c->cursorLetra = c->cursorLetra->ant;
    }
    while(Sugestao != NULL && c->cursorLin->TamLetras < c->MaxCursorX){
        pilha_pop(&Sugestao,&Letra);
        cursor_Frente(c);
        ListaDE_Insere(c,Letra);
    }
}

void tecla_Home(cursor *c){
    /*
    while(c->cursorLin->ant != NULL){
        tecla_Page_Up(c);
    }
    */
    c->cursorLetra = c->cursorLin->inicio;
    c->Posx = c->MinCursorX;
}

void tecla_Del(cursor *c){
    /////letra_Apaga_Frente(c);
    linha *auxLinha = c->cursorLin;
    letras *auxLetra = c->cursorLetra;
    if(c->cursorLetra->prox != NULL && c->cursorLetra->ant != NULL){
        auxLetra->ant->prox = auxLetra->prox;
        auxLetra->prox->ant = auxLetra->ant;
        c->cursorLetra = c->cursorLetra->prox;
        free(auxLetra);
    }
}

void tecla_Insert(cursor *c){
    c->insAtiv = !c->insAtiv;
}

void tecla_End(cursor *c){
    /*CORRIGIDO
    while(c->cursorLin->prox != NULL){
        tecla_Page_Down(c);
    }
    */
    c->cursorLetra = c->cursorLin->fim;
    c->Posx = c->MinCursorX + c->cursorLin->TamLetras;
}

void tecla_Page_Up(cursor *c){
    c->PoslI--;
    c->PoslF--;
    if(c->cursorLin->ant != NULL){
        c->cursorLin = c->cursorLin->ant;
        c->cursorLetra = c->cursorLin->inicio->prox;
        c->Posx = c->MinCursorX + 1;
    }

}

void tecla_Page_Down(cursor *c){
    c->PoslI++;
    c->PoslF++;
    if(c->cursorLin->prox != NULL){
        c->cursorLin = c->cursorLin->prox;
        c->cursorLetra = c->cursorLin->inicio->prox;
        c->Posx = c->MinCursorX + 1;
    }

}

void tecla_Back_Space(cursor *c){
    /////letra_Apaga_Atraz(c);
    linha *auxLinha = c->cursorLin;
    letras *auxLetra = c->cursorLetra->ant;
    if(c->cursorLin->TamLetras <= 2){
        if(c->cursorLin->prox != NULL &&c->cursorLin->ant != NULL){
            c->cursorLin->ant->prox = c->cursorLin->prox;
            c->cursorLin->prox->ant = c->cursorLin->ant;
            c->cursorLin = c->cursorLin->ant;
            c->cursorLetra = c->cursorLin->inicio;
            c->Posy--;
            c->Posx = c->MinCursorX;
            /////
            free(auxLinha);
        }
    }else
    if(c->cursorLetra->ant != NULL && c->cursorLetra->ant->ant != NULL){
        auxLetra->ant->prox = auxLetra->prox;
        auxLetra->prox->ant = auxLetra->ant;
        c->Posx--;
        c->cursorLin->TamLetras--;
        free(auxLetra);
    }
}

void tecla_Seta_Cima(cursor *c){
    if(ListaDE_Navegacao_Cima(c)){
        cursor_Cima(c);
    }else{
        c->Posx = c->MinCursorX + c->cursorLin->TamLetras;
    }
}

void tecla_Seta_Baixo(cursor *c){
    if(ListaDE_Navegacao_Baixo(c)){
        cursor_Baixo(c);
    }else{
        c->Posx = c->MinCursorX;
    }

}

void tecla_Seta_Esquerda(cursor *c){
    if(ListaDE_Navegacao_Esquerda(c))    {
        cursor_Atras(c);
    }else
    if(c->cursorLin->ant != NULL){
        ListaDE_Navegacao_Cima(c);
        cursor_Cima(c);
    }

}

void tecla_Seta_Direita(cursor *c){
    if(ListaDE_Navegacao_Direita(c)){
        cursor_Frente(c);
    }else
    if(c->cursorLin->prox != NULL){
        ListaDE_Navegacao_Baixo(c);
        cursor_Baixo(c);
    }

}

void tecla_Texto(char *Letra,cursor *c){
    ///letra_Mostra(*Letra,c);
    Pilha *Sugestao = NULL ,*Palavra = NULL;
    /////if(c->cursorLetra->prox == NULL || c->insAtiv == 1){
    if(c->insAtiv == 1 && c->cursorLin->TamLetras < c->MaxCursorX){
        free(Sugestao);
        cursor_Frente(c);
        ListaDE_Insere(c,*Letra);
        if(*Letra > 62){
            c->palavraAtiv = 1;
            pilha_push(&c->Palavra,*Letra);
        }else{
            if(c->palavraAtiv){
                c->ListaPalavras = lista_Generalizada_inserir(c->ListaPalavras,pilha_invertePalavra(c->Palavra),c->ListaPalavras);
                /////free(c->Palavra);
                c->palavraAtiv = 0;
                free(c->Palavra);
                c->Palavra = NULL;
            }
        }
        lista_Generalizada_cosultaPalavra(c->ListaPalavras,pilha_copiarPalavra(&c->Palavra),&Sugestao);
        if(Sugestao != NULL){
            tela_Atualiza_Sugestao(pilha_invertePalavra(Sugestao),c);
        }
    }else{
        ListaDE_Insere_Atualiza(c,*Letra);
    }

}

void tecla_Enter(cursor *c){
    if(cursor_Pula_Linha(c)){
        ListaDE_Pula_Linha(c);
    }
}
